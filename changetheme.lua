return {
  name = "Changes The GTK Theme",
  description = "Changes The GTK Theme.",
  author = "David Krawiec",
  version = 0.1,

  onRegister = function(package)
    ide:Print("Theme: ", ide.editorApp:SetNativeTheme(ide.appname.."/../cfg/gtk-themes/Adwaita-dark/gtkrc"))
  end
}